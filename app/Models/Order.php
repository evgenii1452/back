<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property string $id
 * @property string $serviceName
 * @property int $servicePrice
 * @property DateTimeInterface $dateTime
 * @property string $consultantId
 * @property string $clientId
 * @property string $status
 */
class Order extends UUIDModel
{
    use HasFactory;

    const FIELD_ID = 'id',
        FIELD_SERVICE_NAME = 'serviceName',
        FIELD_SERVICE_PRICE = 'servicePrice',
        FIELD_DATE_TIME = 'dateTime',
        FIELD_CONSULTANT_ID = 'consultantId',
        FIELD_CLIENT_ID = 'clientId',
        FIELD_STATUS = 'status';

    public $timestamps = false;

    protected $table = 'TAB_ORDER';

    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_SERVICE_NAME,
        self::FIELD_SERVICE_PRICE,
        self::FIELD_DATE_TIME,
        self::FIELD_CONSULTANT_ID,
        self::FIELD_CLIENT_ID,
        self::FIELD_STATUS,
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(
            Employee::class,
            'consultantId',
            'id'
        );
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(
            Client::class,
            'clientId',
            'id'
        );
    }

    public function cars(): BelongsToMany
    {
        return $this->belongsToMany(
            Car::class,
            'TAB_ORDER_CAR',
            'orderId',
            'carId'
        )->withPivot(['count', 'price']);
    }
}
