<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $id
 * @property string $yearIssue
 * @property string $chassis
 * @property string $carcase
 * @property string $enginePower
 * @property string $maxWight
 */
class Specification extends UUIDModel
{
    use HasFactory;

    const FIELD_ID = 'id',
        FIELD_YEAR_ISSUE = 'yearIssue',
        FIELD_CHASSIS = 'chassis',
        FIELD_CARCASE = 'carcase',
        FIELD_ENGINE_POWER = 'enginePower',
        FIELD_MAX_WIGHT = 'maxWight';

    public $timestamps = false;

    protected $table = 'TAB_SPECIFICATIONS';
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_YEAR_ISSUE,
        self::FIELD_CHASSIS,
        self::FIELD_CARCASE,
        self::FIELD_ENGINE_POWER,
        self::FIELD_MAX_WIGHT,
    ];

    public function cars(): HasMany
    {
        return $this->hasMany(Car::class, 'specificationsId', 'id');
    }
}
