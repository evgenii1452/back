<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property string $id
 * @property string $title
 * @property int $price
 * @property string $specificationsId
 */
class Car extends UUIDModel
{
    use HasFactory;

    const FIELD_ID = 'id',
        FIELD_TITLE = 'title',
        FIELD_PRICE = 'price',
        FIELD_SPECIFICATIONS_ID = 'specificationsId';

    public $timestamps = false;

    protected $table = 'TAB_CAR';
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_TITLE,
        self::FIELD_PRICE,
        self::FIELD_SPECIFICATIONS_ID,
    ];

    public function specification(): BelongsTo
    {
        return $this->belongsTo(
            Specification::class,
            self::FIELD_SPECIFICATIONS_ID
        );
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(
            Order::class,
            'TAB_ORDER_CAR',
            'carId',
            'orderId'
        )->withPivot(['count', 'price']);
    }
}
