<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $id
 * @property string $name
 * @property string $secondName
 * @property string $lastName
 * @property string $gender
 * @property string $phoneNumber
 * @property DateTimeInterface $birthday
 */
class Client extends UUIDModel
{
    use HasFactory;

    const FIELD_ID = 'id',
        FIELD_NAME = 'name',
        FIELD_SECOND_NAME = 'secondName',
        FIELD_LAST_NAME = 'lastName',
        FIELD_GENDER = 'gender',
        FIELD_PHONE_NUMBER = 'phoneNumber',
        FIELD_BIRTHDAY = 'birthday';

    const ENTITY_RELATIVE_ORDERS = 'orders';

    public $timestamps = false;

    protected $table = 'TAB_CLIENT';
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_NAME,
        self::FIELD_SECOND_NAME,
        self::FIELD_LAST_NAME,
        self::FIELD_GENDER,
        self::FIELD_PHONE_NUMBER,
        self::FIELD_BIRTHDAY,
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'clientId');
    }
}
