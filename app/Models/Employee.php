<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $id
 * @property string $secondName
 * @property string $name
 * @property string $lastName
 * @property string $departmentId
 * @property string $phoneNumber
 * @property string $passportNumber
 * @property string $passportSeries
 * @property DateTimeInterface $birthplace
 * @property string $regPlace
 */
class Employee extends UUIDModel
{
    use HasFactory;

    const FIELD_ID = 'id',
        FIELD_SECOND_NAME = 'secondName',
        FIELD_NAME = 'name',
        FIELD_LAST_NAME = 'lastName',
        FIELD_DEPARTMENT_ID = 'departmentId',
        FIELD_PHONE_NUMBER = 'phoneNumber',
        FIELD_PASSPORT_NUMBER = 'passportNumber',
        FIELD_PASSPORT_SERIES = 'passportSeries',
        FIELD_BIRTHPLACE = 'birthplace',
        FIELD_REG_PLACE = 'regPlace';

    public $timestamps = false;

    protected $table = 'TAB_EMPLOYEE';
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_SECOND_NAME,
        self::FIELD_NAME,
        self::FIELD_LAST_NAME,
        self::FIELD_DEPARTMENT_ID,
        self::FIELD_PHONE_NUMBER,
        self::FIELD_PASSPORT_NUMBER,
        self::FIELD_PASSPORT_SERIES,
        self::FIELD_BIRTHPLACE,
        self::FIELD_REG_PLACE,
    ];

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'consultantId');
    }

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'departmentId');
    }
}
