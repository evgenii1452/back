<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

abstract class UUIDModel extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($entity) {
            $entity->{$entity->getKeyName()} = (string) Str::uuid();
        });
    }
    public function getIncrementing(): bool
    {
        return false;
    }
    public function getKeyType(): string
    {
        return 'string';
    }
}
