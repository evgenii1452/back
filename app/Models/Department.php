<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $id
 * @property string $title
 * @property string $place
 */
class Department extends UUIDModel
{
    use HasFactory;

    const FIELD_ID = 'id',
        FIELD_TITLE = 'title',
        FIELD_PLACE = 'place';

    public $timestamps = false;

    protected $table = 'TAB_DEPARTMENT';
    protected $fillable = [
        self::FIELD_ID,
        self::FIELD_TITLE,
        self::FIELD_PLACE,
    ];

    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class, 'departmentId');
    }
}
