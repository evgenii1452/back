<?php

namespace App\Http\Requests\Car;

use App\Models\Car;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            Car::FIELD_PRICE => ['required', 'integer'],
            Car::FIELD_SPECIFICATIONS_ID => ['required', 'exists:TAB_SPECIFICATIONS,id'],
            Car::FIELD_TITLE => ['required', 'string'],
        ];
    }
}
