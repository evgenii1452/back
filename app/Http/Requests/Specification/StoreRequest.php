<?php

namespace App\Http\Requests\Specification;

use App\Models\Specification;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            Specification::FIELD_MAX_WIGHT => ['required', 'string'],
            Specification::FIELD_ENGINE_POWER => ['required', 'string'],
            Specification::FIELD_CARCASE => ['required', 'string'],
            Specification::FIELD_CHASSIS => ['required', 'string'],
            Specification::FIELD_YEAR_ISSUE => ['required', 'string'],
        ];
    }
}
