<?php

namespace App\Http\Requests\Client;

use App\Models\Client;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            Client::FIELD_NAME         => ['string'],
            Client::FIELD_SECOND_NAME  => ['string'],
            Client::FIELD_LAST_NAME    => ['string'],
            Client::FIELD_GENDER       => ['string'],
            Client::FIELD_PHONE_NUMBER => ['string'],
            Client::FIELD_BIRTHDAY     => ['string'],
        ];
    }
}
