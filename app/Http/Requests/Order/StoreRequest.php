<?php

namespace App\Http\Requests\Order;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            Order::FIELD_SERVICE_NAME => ['required', 'string'],
            Order::FIELD_SERVICE_PRICE => ['required', 'integer'],
//            Order::FIELD_DATE_TIME => ['required', 'string'],
            Order::FIELD_CONSULTANT_ID => ['required', 'exists:TAB_EMPLOYEE,id'],
            Order::FIELD_CLIENT_ID => ['required', 'exists:TAB_CLIENT,id'],
            Order::FIELD_STATUS => ['required', 'string'],
            'carId' => ['required', 'exists:TAB_CAR,id'],
            'count' => ['required', 'integer'],

        ];
    }
}
