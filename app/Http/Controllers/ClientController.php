<?php

namespace App\Http\Controllers;


use App\Http\Requests\Client\StoreRequest;
use App\Http\Requests\Client\UpdateRequest;
use App\Http\Resources\ClientResource;
use App\Services\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class ClientController extends Controller
{
    private ClientService $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    public function index(): JsonResponse
    {
        $client = $this->clientService->getAll();

        return response()->json([
            'data' => new ResourceCollection($client)
        ]);
    }

    public function show(string $uuid): JsonResponse
    {
        $client = $this->clientService->getById($uuid);

        return response()->json([
            'data' => new ClientResource($client)
        ]);
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $data = $request->validated();
        $client = $this->clientService->store($data);

        return response()->json([
            'data' => new ClientResource($client)
        ]);
    }

    public function update(string $uuid, UpdateRequest $request): JsonResponse
    {
        $data = $request->all();
        $client = $this->clientService->update($uuid, $data);

        return response()->json([
            'data' => new ClientResource($client)
        ]);
    }


    public function delete(string $uuid): Response
    {
        $this->clientService->delete($uuid);

        return response(null, 204);
    }
}
