<?php

namespace App\Http\Controllers;


use App\Http\Requests\Car\StoreRequest;
use App\Http\Requests\Car\UpdateRequest;
use App\Http\Resources\CarResource;
use App\Models\Car;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class CarController extends Controller
{
    public function index(): JsonResponse
    {
        $cars = Car::query()->get();

        return response()->json([
            'data' => new ResourceCollection($cars)
        ]);
    }

    public function show(string $uuid): JsonResponse
    {
        $car = Car::query()->findOrFail($uuid);

        return response()->json([
            'data' => new CarResource($car)
        ]);
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $car = Car::query()->create($request->all());

        return response()->json([
            'data' => new CarResource($car)
        ]);
    }

    public function update(string $uuid, UpdateRequest $request): JsonResponse
    {
        $car = Car::query()
            ->findOrFail($uuid);

        $car->update($request->all());

        return response()->json([
            'data' => new CarResource($car)
        ]);
    }


    public function delete(string $uuid): Response
    {
        Car::query()
            ->findOrFail($uuid)
            ->delete();

        return response(null, 204);
    }
}
