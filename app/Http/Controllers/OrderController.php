<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\StoreRequest;
use App\Http\Requests\Order\UpdateRequest;
use App\Http\Resources\OrderResource;
use App\Models\Car;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilder;

class OrderController extends Controller
{
    public function index(): JsonResponse
    {
        $orders = QueryBuilder::for(Order::class)->allowedIncludes(['cars'])->get();
//        $orders = Order::query()->->get();

        return response()->json([
            'data' => new ResourceCollection($orders)
        ]);
    }

    public function show(string $uuid): JsonResponse
    {
        $order = QueryBuilder::for(Order::class)
            ->allowedIncludes(['cars'])
            ->where('id', $uuid)
            ->first();
//            ->findOrFail($uuid);

//        $order = Order::query()->findOrFail($uuid);

        return response()->json([
            'data' => new JsonResource($order)
        ]);
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $orderData = $request->only([
            Order::FIELD_SERVICE_NAME,
            Order::FIELD_SERVICE_PRICE,
            Order::FIELD_DATE_TIME,
            Order::FIELD_CONSULTANT_ID,
            Order::FIELD_CLIENT_ID,
            Order::FIELD_STATUS,
        ]);
        $orderData['dateTime'] = Carbon::now();

        $carData = [
            'carId' => $request->get('carId'),
            'count' => $request->get('count'),
            'id' => Str::uuid()->toString(),
            'price' => $request->get(Order::FIELD_SERVICE_PRICE),
        ];

        try {
            DB::beginTransaction();

            /**
             * @var Order $order
             */
            $order = Order::query()->create($orderData);
            $order->cars()->attach($carData['carId'], $carData);

            DB::commit();

            return response()->json([
                'data' => new OrderResource($order)
            ]);
        } catch (\Exception $exception) {
//            DB::rollBack();
        }

        return response()->json([
            'data' => $order ?? []
        ]);
    }

    public function update(string $uuid, UpdateRequest $request): JsonResponse
    {
        $order = Order::query()
            ->findOrFail($uuid);
        $order
            ->update($request->all());

        return response()->json([
            'data' => new OrderResource($order)
        ]);
    }


    public function delete(string $uuid): Response
    {
        $order = Order::query()->findOrFail($uuid);
        $order->cars()->detach();
        $order->delete();

        return response(null, 204);
    }
}
