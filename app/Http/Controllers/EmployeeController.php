<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employee\StoreRequest;
use App\Http\Requests\Employee\UpdateRequest;
use App\Http\Resources\EmployeeResource;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    public function index(): JsonResponse
    {
        $employees = Employee::query()->get();

        return response()->json([
            'data' => new ResourceCollection($employees)
        ]);
    }

    public function show(string $uuid): JsonResponse
    {
        $employee = Employee::query()->findOrFail($uuid);

        return response()->json([
            'data' => new EmployeeResource($employee)
        ]);
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $employee = Employee::query()->create($request->all());

        return response()->json([
            'data' => new EmployeeResource($employee)
        ]);
    }

    public function update(string $uuid, UpdateRequest $request): JsonResponse
    {
        $employee = Employee::query()->findOrFail($uuid);
        $employee->update($request->all());

        return response()->json([
            'data' => new EmployeeResource($employee)
        ]);
    }


    public function delete(string $uuid): Response
    {
        Employee::query()
            ->findOrFail($uuid)
            ->delete();

        return response(null, 204);
    }
}
