<?php

namespace App\Http\Controllers;


use App\Http\Requests\Client\StoreRequest;
use App\Http\Requests\Client\UpdateRequest;
use App\Http\Resources\ClientResource;
use App\Services\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class MenuController extends Controller
{
    public function index(): JsonResponse
    {
        $menu = config('site.menu');

        return response()->json([
            'data' => $menu
        ]);
    }
}
