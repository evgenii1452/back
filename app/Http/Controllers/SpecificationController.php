<?php

namespace App\Http\Controllers;

use App\Http\Requests\Specification\StoreRequest;
use App\Http\Requests\Specification\UpdateRequest;
use App\Http\Resources\SpecificationResource;
use App\Models\Specification;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class SpecificationController extends Controller
{
    public function index(): JsonResponse
    {
        $specifications = Specification::query()->get();

        return response()->json([
            'data' => new ResourceCollection($specifications)
        ]);
    }

    public function show(string $uuid): JsonResponse
    {
        $specification = Specification::query()->findOrFail($uuid);

        return response()->json([
            'data' => new SpecificationResource($specification)
        ]);
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $data = $request->all();
        $data['yearIssue'] = Carbon::createFromDate($data['yearIssue']);
        $specification = Specification::query()->create($data);


        return response()->json([
            'data' => new SpecificationResource($specification)
        ]);
    }

    public function update(string $uuid, UpdateRequest $request): JsonResponse
    {
        $specification = Specification::query()
            ->findOrFail($uuid)
            ->update($request->all());

        return response()->json([
            'data' => new SpecificationResource($specification)
        ]);
    }


    public function delete(string $uuid): Response
    {
        Specification::query()
            ->findOrFail($uuid)
            ->delete();

        return response(null, 204);
    }
}
