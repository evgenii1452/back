<?php

namespace App\Http\Controllers;

use App\Http\Requests\Department\StoreRequest;
use App\Http\Requests\Department\UpdateRequest;
use App\Http\Resources\DepartmentResource;
use App\Models\Department;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class DepartmentController extends Controller
{
    public function index(): JsonResponse
    {
        $departments = Department::query()->get();

        return response()->json([
            'data' => new ResourceCollection($departments)
        ]);
    }

    public function show(string $uuid): JsonResponse
    {
        $department = Department::query()->findOrFail($uuid);

        return response()->json([
            'data' => new DepartmentResource($department)
        ]);
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $department = Department::query()->create($request->all());

        return response()->json([
            'data' => new DepartmentResource($department)
        ]);
    }

    public function update(string $uuid, UpdateRequest $request): JsonResponse
    {
        $department = Department::query()->findOrFail($uuid);
        $department->update($request->all());

        return response()->json([
            'data' => new DepartmentResource($department)
        ]);
    }


    public function delete(string $uuid): Response
    {
        Department::query()
            ->findOrFail($uuid)
            ->delete();

        return response(null, 204);
    }
}
