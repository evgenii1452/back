<?php

namespace App\Http\Resources;

use App\Models\Car;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /**
         * @var Car $car
         */
        $car = $this->resource;

        return [
            Car::FIELD_ID                => $car->id,
            Car::FIELD_TITLE             => $car->title,
            Car::FIELD_SPECIFICATIONS_ID => $car->specificationsId,
            Car::FIELD_PRICE             => $car->price,
        ];
    }
}
