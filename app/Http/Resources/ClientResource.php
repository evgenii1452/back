<?php

namespace App\Http\Resources;

use App\Models\Client;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /**
         * @var Client $client
         */
        $client = $this->resource;

        return [
            Client::FIELD_ID           => $client->id,
            Client::FIELD_NAME         => $client->name,
            Client::FIELD_SECOND_NAME  => $client->secondName,
            Client::FIELD_LAST_NAME    => $client->lastName,
            Client::FIELD_GENDER       => $client->gender,
            Client::FIELD_PHONE_NUMBER => $client->phoneNumber,
            Client::FIELD_BIRTHDAY     => $client->birthday,
        ];
    }
}
