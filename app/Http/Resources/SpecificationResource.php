<?php

namespace App\Http\Resources;

use App\Models\Specification;
use Illuminate\Http\Resources\Json\JsonResource;

class SpecificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /**
         * @var Specification $specification
         */
        $specification = $this->resource;

        return [
            Specification::FIELD_ID           => $specification->id,
            Specification::FIELD_YEAR_ISSUE   => $specification->yearIssue,
            Specification::FIELD_CHASSIS      => $specification->chassis,
            Specification::FIELD_CARCASE      => $specification->carcase,
            Specification::FIELD_ENGINE_POWER => $specification->enginePower,
            Specification::FIELD_MAX_WIGHT    => $specification->maxWight,
        ];
    }
}
