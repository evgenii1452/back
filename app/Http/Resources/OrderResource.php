<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /**
         * @var Order $order
         */
        $order = $this->resource;

        return [
            Order::FIELD_ID            => $order->id,
            Order::FIELD_SERVICE_NAME  => $order->serviceName,
            Order::FIELD_SERVICE_PRICE => $order->servicePrice,
            Order::FIELD_DATE_TIME     => $order->dateTime,
            Order::FIELD_CONSULTANT_ID => $order->consultantId,
            Order::FIELD_CLIENT_ID     => $order->clientId,
            Order::FIELD_STATUS        => $order->status,
//            'include' => $this->include($request)
        ];
    }

//    private function include($request)
//    {
//        $includes = [
//            'cars'      => ResourceCollection::collection($this->whenLoaded('cars')),
////            Product::ENTITY_RELATIVE_ORGANIZATION => OrganizationResource::make($this->whenLoaded(Product::ENTITY_RELATIVE_ORGANIZATION)),
//
//        ];
//
//        if ($request->input('include')) {
//            return $includes;
//        }
//
//        return null;
//
//    }
}
