<?php

namespace App\Http\Resources;

use App\Models\Department;
use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /**
         * @var Department $department
         */
        $department = $this->resource;

        return [
            Department::FIELD_ID    => $department->id,
            Department::FIELD_TITLE => $department->title,
            Department::FIELD_PLACE => $department->place,
        ];
    }
}
