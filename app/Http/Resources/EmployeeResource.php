<?php

namespace App\Http\Resources;

use App\Models\Employee;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /**
         * @var Employee $employee
         */
        $employee = $this->resource;

        return [
            Employee::FIELD_ID              => $employee->id,
            Employee::FIELD_SECOND_NAME     => $employee->secondName,
            Employee::FIELD_NAME            => $employee->name,
            Employee::FIELD_LAST_NAME       => $employee->lastName,
            Employee::FIELD_DEPARTMENT_ID   => $employee->departmentId,
            Employee::FIELD_PHONE_NUMBER    => $employee->phoneNumber,
            Employee::FIELD_PASSPORT_NUMBER => $employee->passportNumber,
            Employee::FIELD_PASSPORT_SERIES => $employee->passportSeries,
            Employee::FIELD_BIRTHPLACE      => $employee->birthplace,
            Employee::FIELD_REG_PLACE       => $employee->regPlace,
        ];
        return parent::toArray($request);
    }
}
