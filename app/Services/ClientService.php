<?php

namespace App\Services;

use App\Models\Client;
use App\Repositories\ClientRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ClientService
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function getAll(): Collection
    {
        return $this->clientRepository->getAll();
    }

    public function getById($uuid): Model|Client
    {
        return $this->clientRepository->getById($uuid);
    }

    public function store(array $data): Model|Client
    {
        return $this->clientRepository->store($data);
    }

    public function update(string $uuid, array $data): Model|Client
    {
        return $this->clientRepository->update($uuid, $data);
    }

    public function delete(string $uuid)
    {
        $this->clientRepository->delete($uuid);
    }
}
