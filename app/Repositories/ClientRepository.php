<?php

namespace App\Repositories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\QueryBuilder\QueryBuilder;

class ClientRepository
{
    public function getAll(): Collection
    {
        return QueryBuilder::for(Client::class)
            ->allowedFilters([
                Client::FIELD_ID
            ])
            ->allowedIncludes([
                Client::ENTITY_RELATIVE_ORDERS,
            ])
            ->get();
    }

    public function getById(string $uuid): Model|Client
    {
        return QueryBuilder::for(Client::class)
            ->allowedIncludes([
                Client::ENTITY_RELATIVE_ORDERS,
            ])
            ->findOrFail($uuid);
    }

    public function store(array $data): Model|Client
    {
        return Client::query()->create($data);
    }

    public function update(string $uuid, array $data): Model|Client
    {
        $client = Client::query()->findOrFail($uuid);
        $client->update($data);

        return $client;
    }


    public function delete(string $uuid): void
    {
        Client::query()
            ->findOrFail($uuid)
            ->delete();
    }
}
