<?php

return [
    'admin' => [
        'Список клиентов' => '/clients',
        'Создать клиента' => '/clients/create',

        'Список сотрудников' => '/employees',
        'Создать сотрудника' => '/employees/create',

        'Список отделов' => '/departments',
        'Создать отдел' => '/departments/create',

        'Список спецификаций' => '/specifications',
        'Создать спецификацию' => '/specifications/create',

        'Список автомобилей' => '/cars',
        'Создать автомобиль' => '/cars/create',

        'Список заказов' => '/orders',
        'Создать заказ' => '/orders/create',
    ],
    'clients' => [
        'Создать заказ' => '/orders/create',
    ],
    'employees' => [
        'Изменить заказ' => '/orders'
    ]
];
