<?php

use App\Http\Controllers\CarController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SpecificationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', [ClientController::class, 'index']);
        Route::get('/{uuid}', [ClientController::class, 'show']);
        Route::post('/', [ClientController::class, 'store']);
        Route::patch('/{uuid}', [ClientController::class, 'update']);
        Route::delete('/{uuid}', [ClientController::class, 'delete']);
    });

    Route::group(['prefix' => 'employees'], function () {
        Route::get('/', [EmployeeController::class, 'index']);
        Route::get('/{uuid}', [EmployeeController::class, 'show']);
        Route::post('/', [EmployeeController::class, 'store']);
        Route::patch('/{uuid}', [EmployeeController::class, 'update']);
        Route::delete('/{uuid}', [EmployeeController::class, 'delete']);
    });

    Route::group(['prefix' => 'cars'], function () {
        Route::get('/', [CarController::class, 'index']);
        Route::get('/{uuid}', [CarController::class, 'show']);
        Route::post('/', [CarController::class, 'store']);
        Route::patch('/{uuid}', [CarController::class, 'update']);
        Route::delete('/{uuid}', [CarController::class, 'delete']);
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', [OrderController::class, 'index']);
        Route::get('/{uuid}', [OrderController::class, 'show']);
        Route::post('/', [OrderController::class, 'store']);
        Route::patch('/{uuid}', [OrderController::class, 'update']);
        Route::delete('/{uuid}', [OrderController::class, 'delete']);
    });

    Route::group(['prefix' => 'specifications'], function () {
        Route::get('/', [SpecificationController::class, 'index']);
        Route::get('/{uuid}', [SpecificationController::class, 'show']);
        Route::post('/', [SpecificationController::class, 'store']);
        Route::patch('/{uuid}', [SpecificationController::class, 'update']);
        Route::delete('/{uuid}', [SpecificationController::class, 'delete']);
    });

    Route::group(['prefix' => 'departments'], function () {
        Route::get('/', [DepartmentController::class, 'index']);
        Route::get('/{uuid}', [DepartmentController::class, 'show']);
        Route::post('/', [DepartmentController::class, 'store']);
        Route::patch('/{uuid}', [DepartmentController::class, 'update']);
        Route::delete('/{uuid}', [DepartmentController::class, 'delete']);
    });

    Route::get('menu', [MenuController::class, 'index']);

});
